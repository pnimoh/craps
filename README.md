# Craps
Game of Craps
	Player rolls two dice.  Each die has 6 faces  that contain 1, 2, 3, 4, 5, or 6 spots. 
The sum of the two upward faces is calculated. If the sum on the first throw is 7 (SEVEN) or 11(YO_LEVEN), 
the player wins. If it is 2 (SNAKE_EYES), 3 (TREY), or 12 (BOX_CARS) on the first throw (called “craps”), 
the player loses. If the sum is 4, 5, 6, 8, 9, or 10 on the first throw, this becomes the player’s “point”. 
To win, the player needs to roll until he/she “makes the point” (that is rolls the point value again). 
The player loses by rolling 7 before making the point.

	We will together implement the Game of Craps that we designed in the previous class
	We will start with two classes: Die and Player
—	A Die has Face value (1-6) and can Roll (we will use random number generator)
—	A Player has 2 dice (objects of class Die) and can Toss (the result of a  toss is the sum of the two dice rolls)
—	A Player also has point
—	May add a third class Game that implements the game rules
	Possible extensions (for you to practice)
—	Allow multiple players in the game
—	The point of the  first player becomes the point of the game
—	Player who loses leaves the game
—	The game ends when only one player remains and the game outcome for this player is determined


