﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameOfCraps
{
    //Prince Nimoh
    //Date: June 19, 2018
    class Game
    {
        //Property for the player who turn it is
        public Player ActivePlayer { get; set; }

        //Property of when the game has a winner
        public bool HasWinner { get; set; }

        //List of players in the game
        public List<Player> Players { get; }

        //Constructor for the game class
        //It takes the number of players playing the game
        public Game(int n)
        {
            Player myPlayer;
            Players = new List<Player>();
            for (int i = 0; i < n; i++)
            {
                myPlayer = new Player();
                myPlayer.Name = "Player " + (i + 1);

                Players.Add(myPlayer);
            }
            HasWinner = false;
            ActivePlayer = Players[0];
        }

        //Method simulates a player playing a turn of the game of craps
        //The outcome of a turn is that the player wins, loses, or gets another turn
        public int PlayTurn()
        {
            int tossResults = ActivePlayer.Toss();

            if (ActivePlayer.Point == 0)
            {
                if (tossResults == 7 || tossResults == 11)//Winner
                {
                    HasWinner = true;

                    ActivePlayer.Message = (tossResults == 7) ? "SEVEN" : "YO_LEVEN";
                    
                }
                else if (tossResults == 2 || tossResults == 3 ||
                         tossResults == 12) //Looser
                {
                    ActivePlayer.PlayerDidLoose = true;

                    //Remove active player from the game
                    Players.Remove(ActivePlayer);

                    //Active player message
                    ActivePlayer.Message = (tossResults == 2) ? "SNAKE_EYES" : 
                                           (tossResults == 3) ? "TREY": "BOX_CARS";

                }
                else
                {
                    ActivePlayer.Point = tossResults;
                    SetPointForGame(tossResults);
                    //Set the point for all players
                }
            }
            else
            {
                if (tossResults == ActivePlayer.Point)
                {
                    HasWinner = true;
                }
                else if (tossResults == 7)
                {
                    ActivePlayer.PlayerDidLoose = true;
                }

            }

            return tossResults;
        }

        //Method sets the point property of all players in the players collection
        private void SetPointForGame(int p)
        {
            foreach (Player player in Players)
            {
                player.Point = p;
            }
        }

        //Method sets the first player object in the players collection as the ActivePlayer
        public void SetActivePlayer()
        {
           
            int indexOfCurrentPlayer = Players.IndexOf(ActivePlayer);

            if (Players.Count > 0)
            {
                if (indexOfCurrentPlayer + 1 == Players.Count) //Check if at the end of the list
                {
                    ActivePlayer = Players.First();
                    
                }
                else //we are not at the end of the list
                {
                    //Set active player to the next player
                    ActivePlayer = Players[indexOfCurrentPlayer + 1];
                }
                
            }
        }

        //Method removes the active player
        public void RemoveActivePlayer()
        {
            Players.Remove(ActivePlayer);
        }
       
    }
}
