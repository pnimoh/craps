﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GameOfCraps
{
    //Prince Nimoh
    //Date: June 19, 2018
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        Game craps;
        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void btnToss_Click(object sender, EventArgs e)
        {
           lblToss.Text = craps.PlayTurn().ToString();

            if (craps.HasWinner)
            {
                lblActivePlayer.Text = "Congrats " + craps.ActivePlayer.Name + 
                                       ", you are the winner!\n" + craps.ActivePlayer.Message;
            }
            else if (craps.ActivePlayer.PlayerDidLoose)
            {
                lblActivePlayer.Text = craps.ActivePlayer.Name + 
                    ", you lost.\n" + craps.ActivePlayer.Message;
                craps.RemoveActivePlayer();
                UpdateListBox();
            }
            else
            {
                lblActivePlayer.Text = craps.ActivePlayer.Name + ", you get another turn.\n" 
                    + "Your point is " + craps.ActivePlayer.Point;
            }
            EndCurrentTurn();
        }

        //Method sets up the windows form controls at the end of the turn
        private void EndCurrentTurn()
        {
            btnToss.Enabled = false;


            if (craps.HasWinner) //End the game if there is a winner
            {
                btnNxtPlayer.Enabled = false;
            }
            else if (craps.Players.Count == 0) //The last player didn't win, so end game
            {
                btnNxtPlayer.Enabled = false;
            }
            else //Allow the next play to play
            {
                btnNxtPlayer.Enabled = true;
            }
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
           
            if (Validator.IsProvided(txtNumberOfPlayers, "Number of players") &&
                Validator.IsPositiveInteger(txtNumberOfPlayers, "Number of players"))
            {
                craps = new Game(Convert.ToInt32(txtNumberOfPlayers.Text));
                PrepareForNewTurn();
                txtNumberOfPlayers.Text = "";
                UpdateListBox();
            }
        }

        //Updates the content of the players list box.
        private void UpdateListBox()
        {
            lstBoxPlayers.Items.Clear();

            //Shut down painting while updating
            lstBoxPlayers.BeginUpdate();

            foreach (Player p in craps.Players)
            {
                lstBoxPlayers.Items.Add(p.Name);
            }
            //Resume painting after updating
            lstBoxPlayers.EndUpdate();
        }

        private void PrepareForNewTurn()
        {
            
            lblToss.Text = "";
            btnToss.Enabled = true;
            btnNxtPlayer.Enabled = false;
            if (craps.ActivePlayer.Point != 0)
            {
                lblActivePlayer.Text = craps.ActivePlayer.Name + ", it's your turn!\n"
                     + "Your point is " + craps.ActivePlayer.Point;
            }
            else
            {
                lblActivePlayer.Text = craps.ActivePlayer.Name + ", it's your turn!";
            }
        }

        private void btnNxtPlayer_Click(object sender, EventArgs e)
        {
            craps.SetActivePlayer();
            PrepareForNewTurn();
        }

        
    }
}
