﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameOfCraps
{
    //Prince Nimoh
    
    //Date: June 19, 2018
    class Player
    {
        private Die yellowDie;
        private Die redDie;
        
        public bool PlayerDidLoose { get; set; }

        public string Name { get; set; }

        public string Message { get; set; }
        public int Point { get; set; }

        public Player()
        {
            yellowDie = new Die();
            redDie = new Die();
            PlayerDidLoose = false;
            Message = "";
        }

        public int Toss()
        {
            return yellowDie.Roll() + redDie.Roll();
        }
    }
}
