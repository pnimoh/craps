﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameOfCraps
{
    //Prince Nimoh
    //Date: June 19, 2018
    class Die
    {
        static private Random rand; //Static random number generator for die class
        const int NUMBER_OF_FACES = 6; //Number of faces of the die
        const int ONE = 1; //

        public Die()
        {
            if (rand == null)
            {
                rand = new Random();
            }
        }

        public int Roll()
        {
            return rand.Next(NUMBER_OF_FACES) + ONE;
        }

    }
}
