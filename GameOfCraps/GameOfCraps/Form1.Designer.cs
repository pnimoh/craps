﻿namespace GameOfCraps
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNumberOfPlayers = new System.Windows.Forms.TextBox();
            this.lblActivePlayer = new System.Windows.Forms.Label();
            this.btnToss = new System.Windows.Forms.Button();
            this.lblToss = new System.Windows.Forms.Label();
            this.btnNxtPlayer = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.lstBoxPlayers = new System.Windows.Forms.ListBox();
            this.gameBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.gameBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Garamond", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(32, 43);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(165, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Enter number of players:";
            // 
            // txtNumberOfPlayers
            // 
            this.txtNumberOfPlayers.Location = new System.Drawing.Point(205, 43);
            this.txtNumberOfPlayers.Margin = new System.Windows.Forms.Padding(4);
            this.txtNumberOfPlayers.Name = "txtNumberOfPlayers";
            this.txtNumberOfPlayers.Size = new System.Drawing.Size(121, 25);
            this.txtNumberOfPlayers.TabIndex = 1;
            // 
            // lblActivePlayer
            // 
            this.lblActivePlayer.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblActivePlayer.Font = new System.Drawing.Font("Garamond", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblActivePlayer.ForeColor = System.Drawing.Color.DarkRed;
            this.lblActivePlayer.Location = new System.Drawing.Point(100, 105);
            this.lblActivePlayer.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblActivePlayer.Name = "lblActivePlayer";
            this.lblActivePlayer.Size = new System.Drawing.Size(378, 64);
            this.lblActivePlayer.TabIndex = 2;
            this.lblActivePlayer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnToss
            // 
            this.btnToss.Enabled = false;
            this.btnToss.Font = new System.Drawing.Font("Garamond", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnToss.Location = new System.Drawing.Point(100, 195);
            this.btnToss.Margin = new System.Windows.Forms.Padding(4);
            this.btnToss.Name = "btnToss";
            this.btnToss.Size = new System.Drawing.Size(138, 65);
            this.btnToss.TabIndex = 3;
            this.btnToss.Text = "Roll Dice";
            this.btnToss.UseVisualStyleBackColor = true;
            this.btnToss.Click += new System.EventHandler(this.btnToss_Click);
            // 
            // lblToss
            // 
            this.lblToss.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblToss.Font = new System.Drawing.Font("Garamond", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblToss.ForeColor = System.Drawing.Color.DarkRed;
            this.lblToss.Location = new System.Drawing.Point(303, 195);
            this.lblToss.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblToss.Name = "lblToss";
            this.lblToss.Size = new System.Drawing.Size(174, 64);
            this.lblToss.TabIndex = 4;
            this.lblToss.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnNxtPlayer
            // 
            this.btnNxtPlayer.Enabled = false;
            this.btnNxtPlayer.Font = new System.Drawing.Font("Garamond", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNxtPlayer.Location = new System.Drawing.Point(100, 296);
            this.btnNxtPlayer.Margin = new System.Windows.Forms.Padding(4);
            this.btnNxtPlayer.Name = "btnNxtPlayer";
            this.btnNxtPlayer.Size = new System.Drawing.Size(138, 65);
            this.btnNxtPlayer.TabIndex = 5;
            this.btnNxtPlayer.Text = "Next turn";
            this.btnNxtPlayer.UseVisualStyleBackColor = true;
            this.btnNxtPlayer.Click += new System.EventHandler(this.btnNxtPlayer_Click);
            // 
            // btnStart
            // 
            this.btnStart.Font = new System.Drawing.Font("Garamond", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStart.Location = new System.Drawing.Point(355, 32);
            this.btnStart.Margin = new System.Windows.Forms.Padding(4);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(123, 47);
            this.btnStart.TabIndex = 6;
            this.btnStart.Text = "Start Game";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // lstBoxPlayers
            // 
            this.lstBoxPlayers.Enabled = false;
            this.lstBoxPlayers.ForeColor = System.Drawing.Color.DarkRed;
            this.lstBoxPlayers.FormattingEnabled = true;
            this.lstBoxPlayers.ItemHeight = 18;
            this.lstBoxPlayers.Location = new System.Drawing.Point(507, 33);
            this.lstBoxPlayers.Name = "lstBoxPlayers";
            this.lstBoxPlayers.Size = new System.Drawing.Size(181, 328);
            this.lstBoxPlayers.TabIndex = 7;
            // 
            // gameBindingSource
            // 
            this.gameBindingSource.DataSource = typeof(GameOfCraps.Game);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(807, 378);
            this.Controls.Add(this.lstBoxPlayers);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.btnNxtPlayer);
            this.Controls.Add(this.lblToss);
            this.Controls.Add(this.btnToss);
            this.Controls.Add(this.lblActivePlayer);
            this.Controls.Add(this.txtNumberOfPlayers);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Garamond", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "Let\'s Play Craps";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gameBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtNumberOfPlayers;
        private System.Windows.Forms.Label lblActivePlayer;
        private System.Windows.Forms.Button btnToss;
        private System.Windows.Forms.Label lblToss;
        private System.Windows.Forms.Button btnNxtPlayer;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.ListBox lstBoxPlayers;
        private System.Windows.Forms.BindingSource gameBindingSource;
    }
}

